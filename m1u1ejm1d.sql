﻿                                                            /*  MODULO 1 - UNIDAD 1 - EJEMPLOS 1 */

-- D
DROP DATABASE IF EXISTS m1u1ejm1d;
CREATE DATABASE m1u1ejm1d;

USE m1u1ejm1d;

-- CREANDO TABLAS

-- PRODUCTOS
CREATE OR REPLACE TABLE PRODUCTOS(
  -- campos
  id_p int AUTO_INCREMENT,
  nombre varchar(100),
  peso float,

  -- claves
  PRIMARY KEY (id_p)
);

-- CLIENTES
CREATE OR REPLACE TABLE CLIENTES(
  -- campos
  id_c int AUTO_INCREMENT,
  nombre varchar(50),
  
  -- claves
  PRIMARY KEY (id_c)
);

-- TLF
CREATE OR REPLACE TABLE TLF(
  -- campos
  id_c int,
  telefono varchar(9),

  -- claves
  PRIMARY KEY (id_c,telefono),

  -- claves ajenas
  CONSTRAINT FKtlfclientes FOREIGN KEY(id_c)
  REFERENCES CLIENTES(id_c)
);

-- compran
CREATE OR REPLACE TABLE compran(
  -- campos
  productos int,
  clientes int,
  fecha date,
  cantidad float,

  -- claves
  PRIMARY KEY (productos,clientes),

  -- claves ajenas
  CONSTRAINT FKcompranproductos FOREIGN KEY(productos)
  REFERENCES PRODUCTOS(id_p),

  CONSTRAINT FKcompranclientes FOREIGN KEY(clientes)
  REFERENCES CLIENTES(id_c)
);