﻿                                                            /*  MODULO 1 - UNIDAD 1 - EJEMPLOS 1 */

-- F
DROP DATABASE IF EXISTS m1u1ejm1f;
CREATE DATABASE m1u1ejm1f;

USE m1u1ejm1f;

-- CREANDO TABLAS

-- PRODUCTOS
CREATE OR REPLACE TABLE PRODUCTOS(
  -- campos
  id_p int AUTO_INCREMENT,
  nombre varchar(100),
  peso float,

  -- claves
  PRIMARY KEY (id_p)
);

-- CLIENTES
CREATE OR REPLACE TABLE CLIENTES(
  -- campos
  id_c int AUTO_INCREMENT,
  nombre varchar(50),
  
  -- claves
  PRIMARY KEY (id_c)
);

-- TLF
CREATE OR REPLACE TABLE TLF(
  -- campos
  id_c int,
  telefono varchar(9),

  -- claves
  PRIMARY KEY (id_c,telefono),

  -- claves ajenas
  CONSTRAINT FKtlfclientes FOREIGN KEY(id_c)
  REFERENCES CLIENTES(id_c)
);

-- TIENDA
CREATE OR REPLACE TABLE TIENDA(
  -- campos
  cod int AUTO_INCREMENT,
  direccion varchar(50),
  
  -- claves
  PRIMARY KEY (cod)
);

-- compran
CREATE OR REPLACE TABLE compran(
  -- campos
  clientes int,
  productos int,
  tienda int,
  cantidad float,
  fecha date,

  -- claves
  PRIMARY KEY (clientes,productos,tienda),

  UNIQUE KEY (productos,tienda),

  -- claves ajenas
  CONSTRAINT FKcompranproductos FOREIGN KEY(productos)
  REFERENCES PRODUCTOS(id_p),

  CONSTRAINT FKcompranclientes FOREIGN KEY(clientes)
  REFERENCES CLIENTES(id_c),

  CONSTRAINT FKcomprantienda FOREIGN KEY(tienda)
  REFERENCES TIENDA(cod)
);