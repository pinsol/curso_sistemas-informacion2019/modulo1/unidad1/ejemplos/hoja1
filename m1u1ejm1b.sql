﻿                                                            /*  MODULO 1 - UNIDAD 1 - EJEMPLOS 1 */

-- B
DROP DATABASE IF EXISTS m1u1ejm1b;
CREATE DATABASE m1u1ejm1b;

USE m1u1ejm1b;

-- CREANDO TABLAS

-- CLIENTES
CREATE OR REPLACE TABLE CLIENTES(
  -- campos
  id_c int AUTO_INCREMENT,
  nombre varchar(50),
  
  -- claves
  PRIMARY KEY (id_c)
);

-- PRODUCTOS
CREATE OR REPLACE TABLE PRODUCTOS(
  -- campos
  id_p int AUTO_INCREMENT,
  nombre varchar(100),
  peso float,
  id_c int,
  fecha date,
  cantidad float,

  -- claves
  PRIMARY KEY (id_p),

  -- claves ajenas
  CONSTRAINT FKproductosclientes FOREIGN KEY(id_c)
  REFERENCES CLIENTES(id_c)
);