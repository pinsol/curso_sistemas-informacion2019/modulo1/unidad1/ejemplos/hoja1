﻿                                                            /*  MODULO 1 - UNIDAD 1 - EJEMPLOS 1 */

/** CREANDO BASE DE DATOS **/
-- A
DROP DATABASE IF EXISTS m1u1ejm1a;
CREATE DATABASE m1u1ejm1a;

USE m1u1ejm1a;

/** FIN DE LA CREACION DE LA BASE DE DATOS **/


/** CREANDO TABLAS **/
-- PRODUCTOS
CREATE OR REPLACE TABLE PRODUCTOS(
  id_p int AUTO_INCREMENT,
  nombre varchar(100),
  peso float,

  PRIMARY KEY (id_p)
);

-- CLIENTES
CREATE OR REPLACE TABLE CLIENTES(
  id_c int AUTO_INCREMENT,
  nombre varchar(50),
  
  PRIMARY KEY (id_c)
);

-- compran
CREATE OR REPLACE TABLE compran(
  productos int,
  clientes int,
  fecha date,
  cantidad float,

  PRIMARY KEY (productos,clientes)
);

/** FIN DE LA CREACION DE LAS TABLAS **/


/** CREANDO LAS RESTRICCIONES **/

-- TABLA compran
ALTER TABLE compran
  ADD CONSTRAINT FKcompranproductos FOREIGN KEY(productos)
  REFERENCES PRODUCTOS(id_p),

  ADD CONSTRAINT FKcompranclientes FOREIGN KEY(clientes)
  REFERENCES CLIENTES(id_c)
;

/** INTRODUCIENDO DATOS **/

-- TABLA PRODUCTOS
INSERT INTO PRODUCTOS VALUES
(1, 'prod1', 2),
(2, 'prod2', 3),
(3, 'prod3', 4),
(4, 'prod4', 5),
(5, 'prod5', 6),
(6, 'prod6', 7),
(7, 'prod7', 8),
(8, 'prod8', 9),
(9, 'prod9', 10),
(10, 'prod10', 11),
(11, 'prod11', 12),
(12, 'prod12', 13),
(13, 'prod13', 14),
(14, 'prod14', 15),
(15, 'prod15', 16),
(16, 'prod16', 17),
(17, 'prod17', 18),
(18, 'prod18', 19),
(19, 'prod19', 20);


-- TABLA CLIENTES
INSERT INTO CLIENTES VALUES
(1, 'cl1'),
(2, 'cl2'),
(3, 'cl3'),
(4, 'cl4'),
(5, 'cl5'),
(6, 'cl6'),
(7, 'cl7'),
(8, 'cl8'),
(9, 'cl9'),
(10, 'cl10'),
(11, 'cl11'),
(12, 'cl12'),
(13, 'cl13'),
(14, 'cl14'),
(15, 'cl15'),
(16, 'cl16'),
(17, 'cl17'),
(18, 'cl18'),
(19, 'cl19'),
(20, 'cl20');


-- TABLA compran
INSERT INTO compran VALUES
(1, 18, '2019-06-18 00:00:00', NULL),
(1, 19, '2019-06-19 00:00:00', NULL),
(2, 18, '2019-06-20 00:00:00', NULL),
(3, 1, '2019-06-04 00:00:00', NULL);